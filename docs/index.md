# 主页

## LLVM 中文教程

> 中文名：底层虚拟机

> 外文名：Low Level Virtual Machine

> 简称：LLVM

> 发源时间：2000年

> 开发者：伊利诺伊大学



LLVM是构架编译器(compiler)的框架系统，以C++编写而成，用于优化以任意程序语言编写的程序的编译时间(compile-time)、链接时间(link-time)、运行时间(run-time)以及空闲时间(idle-time)，对开发者保持开放，并兼容已有脚本。

LLVM计划启动于2000年，最初由美国UIUC大学的Chris Lattner博士主持开展。2006年Chris Lattner加盟Apple Inc.并致力于LLVM在Apple开发体系中的应用。Apple也是LLVM计划的主要资助者。

LLVM已经被Apple、Microsoft、Google、Facebook等各大公司采用。



