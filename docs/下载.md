# 下载

## LLVM 下载

- [LLVM 13.0.0](https://hub.fastgit.org/llvm/llvm-project/releases/tag/llvmorg-13.0.0) 
[win32](https://hub.fastgit.org/llvm/llvm-project/releases/download/llvmorg-13.0.0/LLVM-13.0.0-win32.exe)
[win64](https://hub.fastgit.org/llvm/llvm-project/releases/download/llvmorg-13.0.0/LLVM-13.0.0-win64.exe)

- [LLVM 12.0.1](https://hub.fastgit.org/llvm/llvm-project/releases/tag/llvmorg-12.0.1) 
[win32](https://hub.fastgit.org/llvm/llvm-project/releases/download/llvmorg-12.0.1/LLVM-12.0.1-win32.exe)
[win64](https://hub.fastgit.org/llvm/llvm-project/releases/download/llvmorg-12.0.1/LLVM-12.0.1-win64.exe)

- [LLVM 12.0.0](https://hub.fastgit.org/llvm/llvm-project/releases/tag/llvmorg-12.0.0) 
[win32](https://hub.fastgit.org/llvm/llvm-project/releases/download/llvmorg-12.0.0/LLVM-12.0.0-win32.exe)
[win64](https://hub.fastgit.org/llvm/llvm-project/releases/download/llvmorg-12.0.0/LLVM-12.0.0-win64.exe)



